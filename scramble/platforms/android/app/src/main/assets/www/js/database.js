/**
 * File Name: database.js
 *
 * Revision History:
 *       Sabbir Ahmed, 2018-02-22 : Created
 */


var db;

/**
 * General purpose error handler
 * @param tx The transaction object
 * @param error The error object
 */
function errorHandler(tx, error) {
    console.error("SQL Error: " + tx + " (" + error.code + ") : " + error.message);
}



var DB = {
    createDatabase: function(){
        var name= "FlagDB";
        var version = "1.0";
        var displayName = "DB for FlagDB app";
        var size =  2 * 1024 * 1024;

        function successCreate() {
            console.info("Success: Database created successfully");
        }

        db = openDatabase(name, version, displayName, size, successCreate);
    },
    createCountries: function(){
        function txFunction(tx) {
            var sql = "CREATE TABLE IF NOT EXISTS countries( " +
                "countryId INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "countryName VARCHAR(20) NOT NULL);";

            var options = [];

            function successCreate() {
                console.info("Success: Table created successfully.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    fillCountries: function() {
        function txFunction(tx) {
            var sql = "INSERT INTO countries (countryName) VALUES ('Canada')," +
                "('Japan')," +
                "('France')," +
                "('Germany')," +
                "('China')," +
                "('Denmark')," +
                "('Finland')," +
                "('Netherlands')," +
                "('New-Zealand')," +
                "('Norway')," +
                "('Switzerland')," +
                "('Iceland')," +
                "('Bermuda')," +
                "('Australia')," +
                "('South-Korea')," +
                "('USA')," +
                "('Latvia')," +
                "('Sweden');";

            var options = [];

            function successCreate() {
                console.info("Success: Table created successfully.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
        }
        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    dropCountries: function() {
        function txFunction(tx) {
            var sql = "DROP TABLE IF EXISTS countries;";

            var options = [];

            function successCreate() {
                console.info("Success: Table created successfully.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
        }
        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    createFlags: function(){
        function txFunction(tx) {
            var sql = "CREATE TABLE IF NOT EXISTS flags( " +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "countryName VARCHAR(20) NOT NULL," +
                "countryFlagImage VARCHAR(255) NOT NULL); ";

            var options = [];

            function successCreate() {
                console.info("Success: Table created successfully.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    fillFlag: function() {
        function txFunction(tx) {
            var sql = "INSERT INTO flags (countryName, countryFlagImage) VALUES ('Canada','img/canada.png')," +
                "('Japan','img/Japan.png')," +
                "('France','img/france.png')," +
                "('Germany','img/germany.jpg')," +
                "('China','img/China.jpg')," +
                "('Denmark','img/Denmark.png')," +
                "('Finland','img/Finland.png')," +
                "('Netherlands','img/Netherlands.png')," +
                "('New-Zealand','img/New_Zealand.png')," +
                "('Norway','img/Norway.png')," +
                "('Switzerland','img/Switzerland.png')," +
                "('Iceland','img/iceland.png')," +
                "('Bermuda','img/bermuda.png')," +
                "('Australia','img/australia.png')," +
                "('South-Korea','img/southkorea.png')," +
                "('USA','img/usa.png')," +
                "('Latvia','img/latvia.png')," +
                "('Sweden','img/sweden.png');";

            var options = [];

            function successCreate() {
                console.info("Success: Table created successfully.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
        }
        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    dropFlags: function() {
        function txFunction(tx) {
            var sql = "DROP TABLE IF EXISTS flags;";

            var options = [];

            function successCreate() {
                console.info("Success: Table created successfully.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
        }
        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    createTables: function(){
        function txFunction(tx) {
            var sql = "CREATE TABLE IF NOT EXISTS friend( " +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "name VARCHAR(20) NOT NULL, " +
                "fullName VARCHAR(20), " +
                "dob DATE, " +
                "isFriend VARCHAR(1));";

            var options = [];

            function successCreate() {
                console.info("Success: Table created successfully.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    dropTables: function () {

        function txFunction(tx) {
            var sql = "DROP TABLE IF EXISTS items;";
            var options = [];
            function successDrop() {
                console.info("Success: table dropped successfully");
            }
            tx.executeSql(sql, options, successDrop, errorHandler);
        }
        function successTransaction() {
            console.info("Success: drop table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    dropUsers: function () {

        function txFunction(tx) {
            var sql = "DROP TABLE IF EXISTS user;";
            var options = [];
            function successDrop() {
                console.info("Success: table dropped successfully");
            }
            tx.executeSql(sql, options, successDrop, errorHandler);
        }
        function successTransaction() {
            console.info("Success: drop table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    createCartTable: function(){
        function txFunction(tx) {
            var sql = "CREATE TABLE IF NOT EXISTS cart( " +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "userId VARCHAR(20) NOT NULL, " +
                "itemId VARCHAR(255) NOT NULL, " +
                "quantity int NOT NULL);";

            var options = [];

            function successCreate() {
                console.info("Success: Table created successfully.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    createListTable: function(){
        function txFunction(tx) {
            var sql = "CREATE TABLE IF NOT EXISTS items( " +
                "sellerid INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "name VARCHAR(20) NOT NULL, " +
                "description VARCHAR(255), " +
                "price DOUBLE, " +
                "images LONGTEXT, " +
                "quantity INT);";

            var options = [];

            function successCreate() {
                console.info("Success: Table created successfully.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    createUserTable: function(){
        function txFunction(tx) {
            var sql = "CREATE TABLE IF NOT EXISTS user( " +
                "userid INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "name VARCHAR(20) NOT NULL, " +
                "password VARCHAR(255), " +
                "email VARCHAR(255));";

            var options = [];

            function successCreate() {
                console.info("Success: Table created successfully.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    fillCartList: function(){
        function txFunction(tx) {
            var sql = "INSERT INTO cart (userId, itemId,quantity) VALUES ('1','2', 1);";
            var options = [];

            function successCreate() {
                console.info("Success: Table created successfully.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    fillUserList: function(){
        function txFunction(tx) {
            var sql = "INSERT INTO user (name, password, email) VALUES ('ryan', 'Teaching321', 'rhino_777@gmail.com');";
            var options = [];

            function successCreate() {
                console.info("Success: Table created successfully.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    fillListTable: function(){
        function txFunction(tx) {
            var sql = "INSERT INTO items (name, description, price, images, quantity) VALUES ('Xbox One S', 'This is one of the newer xbox console which is almost half the size of the original xbox one', 259.99,'img/Thor-icon.png', 5)," +
                "('HTC Vive', 'The htc vive is one of the hotest vr machines up to date', 699.00,'img/Thor-icon.png', 3)," +
                "('NVIDIA SHIELD', 'This a older released console which allows you to play steam games right to your tv', 389.99, 'img/Thor-icon.png', 2)," +
                "('Nintendo Switch', 'Newer installment', 399.99,'img/Thor-icon.png', 7);";
                //"                \"                \\\"('Sony Playstation 4 Slim', 'This console has a 1TB hard drive allowing you to experience all the games they like.', 349.00, 16);\\\"";

            var options = [];

            function successCreate() {
                console.info("Success: Table created successfully.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    }

};





















