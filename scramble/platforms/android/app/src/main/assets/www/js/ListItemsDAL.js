/**
 * File Name: avengersDAL.js
 *
 * Revision History:
 *       Sabbir Ahmed, 2018-02-22 : Created
 */
var myFlags={
    selectAllFlags: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM flags;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: selectAll transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectAllCountries: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM countries;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: selectAll transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    }

};

var myList ={
    insert: function (options, callback) {
        function txFunction(tx) {
            var sql = "INSERT INTO friend(name, fullName, dob, isFriend) VALUES(?,?,?,?);";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: Insert transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    update: function (options, callback) {
        function txFunction(tx) {
            var sql = "UPDATE friend SET name = ?, fullName = ?, dob = ?, isFriend = ? WHERE id=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: update transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    delete: function (options, callback) {
        function txFunction(tx) {
            var sql = "DELETE FROM friend WHERE id=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: delete transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    select: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM friend WHERE id=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectAll: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM items;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: selectAll transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    }
};

var myLogin={

    select: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM user WHERE email=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }

        function successTransaction() {
            console.info("Success: select transaction successful");
        }

        db.transaction(txFunction, errorHandler, successTransaction);
    },
    checkPassword: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM user WHERE userid=? AND password=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }

        function successTransaction() {
            console.info("Success: select transaction successful");
            //localStorage.setItem("myBool", "true");
        }

        db.transaction(txFunction, errorHandler, successTransaction);
    }

};