let randomNums;

window.addEventListener("load", function(e) {
  localStorage.clear();
  randomNums = _.shuffle(_.range(words.length));
  createScramble();
});

function createScramble() {
  let randomNum = 0;

  if (localStorage.getItem("randomPos") == "" || localStorage.getItem("randomPos") == null) {
    randomNum = randomNums[0];
    localStorage.setItem("randomPos", "0");
    populateBoard(randomNum);
  } else if (parseInt(localStorage.getItem("randomPos")) < words.length - 1) {
    randomNum = randomNums[parseInt(localStorage.getItem("randomPos")) + 1];
    localStorage.setItem("randomPos", parseInt(localStorage.getItem("randomPos")) + 1);
    populateBoard(randomNum);
  } else {
    alert("you have won the game");
    localStorage.clear();
    randomNums = _.shuffle(_.range(words.length));
    resetBoard();
  }

}

function populateBoard(randomNum) {
  const randomWord = words[randomNum];
  localStorage.setItem("wordAnswer", randomWord.wordName);
  //this is to create the empty buttons
  var inputs = "";
  for (var i = 0; i < randomWord.wordName.length; i++) {
    if (randomWord.wordName[i] == " ") {
      inputs += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    } else {
      inputs += '<button class="input" id=' + i + ' style="width: 45px; height:45px;margin-left: 10px; border: 2px solid #37629c; border-radius: 5px;background-color: #b4eaea;">&nbsp;</button>';
    }
  }

  $("#inputs").html($("#inputs").html() + inputs);

  //this is to create the scrambled word
  var newWord = "";
  var count = 0;
  var lengthCounter = 0;

  let wordName = randomWord.wordName.replace(' ', '')


  const mixWord = _.shuffle(wordName.split('')).map(char => {
    if (lengthCounter >= 6) {
      newWord += "<br/><br/>";
      lengthCounter = 0;
    }
    newWord += '<button class= "character" id =' + count + ' value = ' + count + ' style="width: 45px; height:45px;margin-left: 10px; border: 2px solid #37629c; border-radius: 5px;background-color: #b4eaea;">' + char + '</button>';
    count++;
    lengthCounter++;
  });

  const alphebet = 'abcdefgihjklmnopqrstuvwxyz';
  for (var i = 0; i < 5; i++) {
    if (lengthCounter >= 6) {
      newWord += "<br/><br/>";
      lengthCounter = 0;
    }
    newWord += '<button class= "character" id =' + count + ' value = ' + count + ' style="width: 45px; height:45px;margin-left: 10px; border: 2px solid #37629c; border-radius: 5px;background-color: #b4eaea;">' + alphebet[Math.floor(Math.random() * alphebet.length)] + '</button>';
    count++;
    lengthCounter++;
  }

  $("#wordScambled").html($("#wordScambled").html() + newWord);

  $("#answerImage").attr("src", randomWord.src);

  initializePage();
}

function initializePage() {
  $(".character").on("click", function() {
    if ($(this).html().replace(' ', '') != "" && $(this).html().replace(' ', '') != null) {
      characterClick($(this).html(), "input", this.id, $(this).val());
    }
  });

  $(".input").on("click", function() {
    if ($(this).html().replace(' ', '') != "" && $(this).html().replace(' ', '') != null) {
      characterClick($(this).html(), "character", this.id, $(this).val());
    }
  });

}

function characterClick(character, location, position, value) {
  var isTrue = false;
  $("." + location).each(function(index) {
    if (location == "character") {
      if ($(this).val() == value && isTrue == false) {
        $(this).html(character);
        $(this).val(value);
        deleteCharButton(character, location, position);
        isTrue = true;
      }
    } else {
      if (($(this).html().replace(/\s/g, '').replace("&nbsp;", "") == "" || $(this).html() == null) && isTrue == false) {
        $(this).html(character);
        $(this).val(value);
        deleteCharButton(character, location, position);
        isTrue = true;
        if (checkAnswer()) {
          resetBoard();
        }
      }
    }
  });
}

function deleteCharButton(character, location, position) {
  if (location == "input") {
    location = "character";
  } else {
    location = "input";
  }

  var isTrue = false;
  $("." + location).each(function(index) {
    if ($(this).html() == character && this.id == position && isTrue == false) {
      $(this).html("&nbsp;");
      isTrue = true;
    }
  });
}

function checkAnswer() {
  var isTrue = true;
  var word = "";
  $(".input").each(function(index) {
    if ($(this).html() == "" || $(this).html() == null) {
      isTrue = false;
    } else {
      word += $(this).html();
    }
  });

  if (isTrue) {
    const answer = localStorage.getItem("wordAnswer");
    if (word == answer.replace(" ", "")) {
      alert("correct!");
      return true;
    }
    return false;
  }
}

function resetBoard() {
  $(".input").each(function(index) {
    $(this).remove();
  });

  $(".character").each(function(index) {
    $(this).remove();
  });

  $("#wordScambled").html("");
  createScramble();
}
